import ballerina/io;
import ballerina/http;

type UserDetail record {|
    string username;
    string lastname;
    string firstname;
|};

UserDetail[] all_users = [];

service /users on new http:Listener(8080) {
    resource function get all() returns UserDetail[] {
        io:println("handling GET request to /users/all");
        return all_users;
    }

    resource function post insert(@http:Payload UserDetail new_user) returns json {
        io:println("handling POST request to /users/insert");
        all_users.push(new_user);
        return {done: "Ok done"};
    }
}
